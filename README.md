# Plywood-PCB



## Purpose of this repository

This repository is here to store the work of Arnav Joshi, Onni Poikkeus And Essi Räisänen, who worked under the supervision of Karl Mihhels to create plywood-based PCB during autumn 2021 in the course CHEM-E2140: Cellulose-based fibers organized by Professor Eero Kontturi.

The work is licensed under CC BY 4.0 -license, as recommended by Aalto University guidelines. Further use should be appropriately acknowledged.

## Repository structure

Repository is organized under 3 subfolders.

Media: Contains the additonal photos and videos taken during the course

Reports: Contains the final presentation given as a part of the coursework

Guidelines: Contains the detailed instructions on how the work was accomplished.

## Project status

This repository is currently upkept by Karl Mihhels, and is in dormant status. If you wish to restart development, feel free to contact Karl at (karl.mihhels [at] aalto.fi)
