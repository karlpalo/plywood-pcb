
# Project description
In this project, two insulator materials thin 3-layered plywood with a thickness of 2.0 mm and masonite with a thickness of 3.0 mm, were chosen. A thin copper tape, copper sheets with a thickness of 0.1 mm, and electrically conductive paint by Bare Conductive® [10] were chosen
as the conductive surfaces. The milling operations were conducted in Aalto Fablab using a Roland SRM-20 milling machine. The PCB model for milling was designed by the instructor Karl Mihhels. The model was designed such that it had spots for the electrical inlet, switch, resistor (1 kΩ), and LED.
![plot](./guidelineimages/routing_circuit_diagram.jpg)
Routing circuit diagram
![plot](./guidelineimages/trace_milled_path.jpg)
trace milled path

The project was divided into two parts on the basis of attachment methods used for the PCB
components: 1) by soldering and 2) by using conductive paint.

## Copper surface and soldering
In experimental part 1 where electrical components were attached by soldering, the PCB
formation can be divided into three different steps: 1) attaching the conductive surface to the insulator material using different adhesives, 2) milling and drilling the traces to the conductive surface according to the routing circuit diagram, and 3) attaching the components by soldering. On measuring the resistance of the thin copper plate and copper tape it was observed that these surfaces have practically zero resistance.

![plot](./guidelineimages/layer_model.jpg)
Figure of the Step-by-step procedure for attachment of copper plate on the insulating material. 1) insulator material (plywood or masonite), 2) adhesive layer spread on top of insulator material, and 3) attachment of conductive surface (copper plate) to the insulator with adhesive.


To attach the conductive surface, three different types of glues were used: two cyanoacrylatebased super glues Express Lim and Express Gel, and silicon-based glue all made by Casco.
Express Lim glue was more like a low viscous paste that dried very fast and Express Gel glue had a gel-like appearance with less flowability. Both Express Lim and Express Gel were transparent. Silicon glue was black coloured with a high thermal resistance up to 300℃.

In the first experiment, the copper tape was attached to both plywood and masonite surfaces. The tape structure was easily wrinkled because of the thinness and therefore spatula was used to smooth down the surface. As no extra adhesive was required to attach the copper tape to the insulator, the samples were milled immediately after attaching the tape.

In the second experiment Express Lim and Express Gel were spread on the masonite and the plywood, and copper sheets were attached to these surfaces. After 15 minutes of waiting time, it was ensured that the adhesive attached to the components shows complete dryness. The surfaces of the samples were then milled using a 0.2 mill size V-shaped milling tip to 0.10 – 0.25 mm depth to ensure uniform and completely copper-free tracks on the PCB. Finally, the required components were attached to the copper surface with soldering at temperatures
ranging between 280 – 310℃.

## conductive paint
The electrically conductive paint purchased from Bare Conductive was made out of carbon and graphite. The supplier promises that the paint is non-toxic, water-soluble, dry at room temperature and no special curing is required. The paint had a density of 1.16 g/ml and sheet
resistance of 55 Ω/m2 for a film with 50 μm thickness. In comparison with the copper sheets used, the resistance was high.

In the experimental part 2 with conductive paint, reverse milling with the preciously used milling traces on plywood and masonite with a milling tip size of 0.10 mill with a V-shaped end of the tip was carried out. In reverse-milling, the grooves were milled directly on the
surfaces of insulating materials prior to the application of conductive paint. Different depths of traces were milled: 0.1 mm, 0.2 mm, 0.3 mm, 0.4 mm and 0.5 mm. The conductive paint was then spread onto the milled surface with a rubber doctor blade to ensure that paint was spread
uniformly and covered all the tracks equally. The paint was allowed to dry for 15–20 minutes at room temperature. The excess paint can be then removed using an electric sander or manually by using sandpaper. As the excess paint was taken off, the conductive area only remained in the milling traces. Finally, PCB components were attached to the surface using a drop of the same conductive paint.

## Results
The milling operations for all the PCBs were carried out using Aalto-FabLab’s Roland SRM-20 CNC-mill. The results and discussion for the different experiments performed using a copper plate and tape, conductive paints such as the effects of milling on conductive material
surfaces, on different adhesives used, the effects on reverse milling on masonite and plywood are explained in the sections below. The actual working of PCB can be observed along with the advantages and disadvantages for both these methods.

### milling on conductive materials

In all of the three cases the insulating base material used was plywood. The conductive materials are copper sheets attached with cyanoacrylate-based adhesives (left), copper tape, and conductive paint layer.

![plot](./guidelineimages/conductive_layers_plywood.jpg)
Picture: PCB conductive layers on plywood after milling. From left to right: copper sheet with Express Lim (cyanoacrylate glue), copper tape, and conductive paint layer.

### Milling on copper plate attached to plywood with different adhesives

The conductive copper sheets were attached to the plywood using different glues at room temperatures (25℃). Prior to the machining, drying of adhesives was carried out for 15 − 20 minutes without using any specific drying equipment at room temperature. The adhesives used were cyanoacrylate-based Express Lim and Express Gel, and silicon glue.

![plot](./guidelineimages/glues_on_plywood.jpg)
Picture: PCB conductive layer glue types on plywood base after milling. From left to right: Express Lim, Express Gel and Silicon glue.


### Reverse milling on insulator materials

The reverse-milling was utilized on both plywood- and masonite-based insulating materials and the results are seen in picture below. In the case of reverse-milling, the PCB grooves were milled directly on the plywood and masonite at different groove depths.

![plot](./guidelineimages/base_materials.jpg)
Picture: PCB base plate materials – Plywood (left) and Masonite (right) after reverse-milling with groove depth of 0.4 mm.

### Attaching component and powering the circuits

In the case of experiment 1, the soldering of components (resistor, press button, LED, battery connector) on the copper plate was carried out at temperatures between 280 – 310 ℃. The functioning of PCB with soldered components can be observed in picture below.
![plot](./guidelineimages/copper_pcb.jpg)

 The circuit was powered using an external 9V power supply. On one occasion, a copper trace came loose on the surface while attaching the power pins to the PCB. Other than this, the soldering and usability of components on a plywood-based PCB was similar to that of conventional epoxy-based PCBs.

In the case of experiment 2, electrically conductive paint was used to attach the components to the milled plywood containing conductive paint. The functioning of conductive paint–plywood PCB for experiment 2 is shown in picture below

![plot](./guidelineimages/paint_pcb.jpg)


A video of the conductive paint-plywood based PCB in action can be found in this folder.



References:
Electric Paint – Bareconductive.com. [online]. [Accessed on 11 December 2021].
https://www.bareconductive.com/collections/electric-paint.
